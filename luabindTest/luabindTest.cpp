// luabindTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
extern "C" {
#include "..\luabind\lua\lualib.h"
#include "..\luabind\lua\lua.h"
#include "..\luabind\lua\lauxlib.h"
};
#include "..\luabind\luabind\luabind.hpp"
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

void normal_lua()
{
    lua_State* L = lua_open();

    luaL_openlibs(L);

    /* 运行脚本 */
    luaL_dofile(L, "test.lua");

    //获取lua中的showinfo函数  
    lua_getglobal(L, "showinfo");
    //cpp 调用无参数的lua函数，无返回值  
    lua_pcall(L, 0, 0, 0);
    //主动清理堆栈，也可以不调用  



    const char * pstr = "世界如此美好";
    lua_getglobal(L, "showstr");
    lua_pushstring(L, pstr);
    //cpp 调用一个参数的的lua函数，无返回值  
    lua_pcall(L, 1, 0, 0);


    lua_getglobal(L, "add");
    //参数从左到右压栈  
    lua_pushinteger(L, 2);
    lua_pushinteger(L, 3);
    lua_pcall(L, 2, 1, 0);
    printf("lua add function return val is %d \n", lua_tointeger(L, -1));

    lua_close(L);

}

void test(const char* str)
{
    printf("call from lua: %s\n", str);
}



class ExportClass
{
public:
    ExportClass()
    {
        printf("ExportClass init\n");
    }
    virtual ~ExportClass()
    {
        printf("ExportClass destory\n");
    }

    static ExportClass* instance()
    {
        static ExportClass _ec;
        return &_ec;
    }

    void call(int a, const char* b)
    {
        printf("call exportClass: %08x %d %s\n", this, a, b);
    }

    int getValue()
    {
        return 10;
    }

    std::string getString()
    {
        return "from c++ string";
    }

    void reg(lua_State* L)
    {
        

    }
};


ExportClass* GetObject(ExportClass* a)
{
    a->call(10, "this is lua object A");
    return ExportClass::instance();
}

void luabind_test()
{
    lua_State* L = lua_open();
    luaL_openlibs(L);
    luabind::open(L);
   // object_ptr = boost::make_shared<ExportClass>();
    
    try {
        luabind::module(L)
            [
                luabind::def("GetObject", &GetObject),
                luabind::class_<ExportClass, ExportClass*>("ExportClass")
                .def(luabind::constructor<>())
                .def("call", &ExportClass::call)
                .def("getValue", &ExportClass::getValue)
                .def("getString", &ExportClass::getString)
            ];

        int err = luaL_dofile(L, "test2.lua");
        if (err) {
            printf("Errors: %s\n", lua_tostring(L, -1));
            lua_pop(L, 1);
        }
        //luabind::call_function<void>(L, "TestCall", boost::ref(this));
    } catch (luabind::error& e) {
        printf("error: %s\n", e.what());
    }

    lua_close(L);
}

int _tmain(int argc, _TCHAR* argv[])
{
    //normal_lua();
    luabind_test();
    return 0;
}

